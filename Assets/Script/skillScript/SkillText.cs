﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillText : MonoBehaviour
{
    public Text txt;
    int cooldown;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<skillScript>();
    }

    // Update is called once per frame
    void Update()
    {
        cooldown = (int)skillScript.waitTime;
        if (cooldown >= 2)
        {
            txt.text = null;
        }
        else {            
            txt.GetComponent<Text>().text = cooldown.ToString();
        }
    }
}
