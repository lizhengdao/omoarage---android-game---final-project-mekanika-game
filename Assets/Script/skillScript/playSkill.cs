﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playSkill : MonoBehaviour
{
    //MOVING
    public float speed;

    //ATTACK
    public static float power = 8.5f;
    private float distanceToClosestEnemy;
    private Enemy1Script closestEnemy;
    private Enemy1Script[] allEnemies;

    //touch
    Touch touch;
    Vector3 touchPosition;


    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Enemy1Script>();
        GetComponent<MoveScript>();
    }

    // Update is called once per frame
    void Update()
    {
        FindClosestEnemy();
    }

    void FindClosestEnemy()
    {
        distanceToClosestEnemy = Mathf.Infinity;
        closestEnemy = null;
        allEnemies = GameObject.FindObjectsOfType<Enemy1Script>();

        foreach (Enemy1Script currentEnemy in allEnemies)
        {
            float distanceToEnemy = (currentEnemy.transform.position - this.transform.position).sqrMagnitude;
            if (distanceToEnemy < distanceToClosestEnemy)
            {
                distanceToClosestEnemy = distanceToEnemy;
                closestEnemy = currentEnemy;
            }
        }

        transform.position = Vector2.MoveTowards(transform.position, closestEnemy.transform.position, speed * Time.deltaTime);
        if (transform.position.x == closestEnemy.transform.position.x && transform.position.y == closestEnemy.transform.position.y)
        {
            DestroyProjectile();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("EnemyTroops"))
        {
            //decrease health

            other.gameObject.GetComponent<Enemy1Script>().health -= power;
            power += 0.2f;
            DestroyProjectile();

            /* FEEDBACK
            float lifeTime = 2;
            MoveScript.moveSpeed += 1;

            if (MoveScript.moveSpeed >= 10)
            {
                MoveScript.moveSpeed = 10;
            }

            lifeTime -= Time.deltaTime; 
            if(lifeTime <= 2 && lifeTime >= 0)
            {
                EnemyPlayer.energi -= power;
            }            

            DestroyProjectile();
            Debug.Log(power);


            Enemy1Script.startTimeBtwShots -= 1;
            if (Enemy1Script.startTimeBtwShots <= 1)
            {
                Enemy1Script.startTimeBtwShots = 1;
            }
            */
        }
    }

    void DestroyProjectile()
    {
        Destroy(gameObject);
    }
}
