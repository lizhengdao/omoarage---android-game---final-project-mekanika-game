﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class skill2Text : MonoBehaviour
{
    public Text txt;
    int cooldown;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<skillScript2>();
    }

    // Update is called once per frame
    void Update()
    {
        cooldown = (int)skillScript2.waitTime;
        if (cooldown >= 5)
        {
            txt.text = null;
        }
        else
        {
            txt.GetComponent<Text>().text = cooldown.ToString();
        }
    }
}
