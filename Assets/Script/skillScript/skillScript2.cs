﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skillScript2 : MonoBehaviour
{
    public static bool skillActive = false;
    public static float waitTime;
    public float startWaitTime = 5;
    public static bool cooldown = true;

    // Start is called before the first frame update
    void Start()
    {
        waitTime = startWaitTime;
        GetComponent<MoveScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if (cooldown == true)
        {
            return;
        }

        else
        {
            if (waitTime <= 0)
            {
                waitTime = startWaitTime;
                cooldown = true;
            }

            else
            {
                skillActive = false;
                waitTime -= Time.deltaTime;
            }
        }
    }

    public void startSkill()
    {
        //FEEDBACK SPEED SLOW
        if (cooldown == true)
        {
            Debug.Log("Start");
            skillActive = true;
            MoveScript.mana -= 2;
        }

        else
        {
            Debug.Log("Skill not ready");
        }
    }
}
