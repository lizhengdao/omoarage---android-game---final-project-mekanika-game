﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LeaderClick : MonoBehaviour
{
    public void LeaderGameButton()
    {
        SceneManager.LoadScene("3");
    }

    public void BacktoMenu()
    {
        SceneManager.LoadScene("MainMenuScene");
    }
}
