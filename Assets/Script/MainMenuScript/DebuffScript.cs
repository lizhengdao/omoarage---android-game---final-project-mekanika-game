﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebuffScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {   
        GetComponent<MoveScript>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            MoveScript.moveSpeed -= 2;
            Destroy(this.gameObject);
        }
    }
}
