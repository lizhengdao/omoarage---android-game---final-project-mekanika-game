﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SettingClick : MonoBehaviour
{
    public void SettingGameButton()
    {
        SceneManager.LoadScene("2");
    }

    public void BacktoMenu()
    {
        SceneManager.LoadScene("MainMenuScene");
    }
}
