﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AboutClick : MonoBehaviour
{
    public void AboutGameButton()
    {
        SceneManager.LoadScene("4");
    }

    public void BacktoMenu()
    {
        SceneManager.LoadScene("MainMenuScene");
    }
}
