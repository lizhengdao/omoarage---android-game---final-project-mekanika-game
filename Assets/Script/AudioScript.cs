﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScript : MonoBehaviour
{
    public AudioClip musicClip;
    public AudioSource musicSource;
    public static bool onAudio = true;   
    // Start is called before the first frame update
    private void Start()
    {
        if (musicSource == true)
        {
            musicSource.clip = musicClip;
            musicSource.Play();
            DontDestroyOnLoad(this.gameObject);
        }

        if(onAudio == false)
        {
            musicSource.Stop();
        }        
    } 

    // Update is called once per frame
    void Update()
    {
        
    }
}
