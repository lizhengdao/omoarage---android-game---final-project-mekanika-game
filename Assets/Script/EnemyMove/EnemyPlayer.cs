﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyPlayer : MonoBehaviour
{
    Image energiValue;
    float maxStorage = 100f;

    public static float energi;

    public Text energn;
    // Start is called before the first frame update
    void Start()
    {
        energiValue = GetComponent<Image>();
        energi = maxStorage;
    }

    // Update is called once per frame
    void Update()
    {
        energiValue.fillAmount = energi / maxStorage;
        energn.text = energi.ToString();
    }
}
