﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1Script : MonoBehaviour
{
    //OBJECT
    private Transform player;
    private Animator anim;
    public float health = 100;

    //MOVING
    public float speed;
    public float stoppingDistance;
    public float retreatDistance;

    //PATROL
    public float waitTime;
    public float startWaitTime;
    public float followingPlayer;
    public Transform[] moveSpots;
    private int randomSpot;

    //SHOOTING
    public float timeBtwShot;
    public static float startTimeBtwShots = 2;
    public GameObject projectile;
     
    // Start is called before the first frame update
    void Start()
    {
        waitTime = startWaitTime;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        timeBtwShot = startTimeBtwShots;
        randomSpot = Random.Range(0, moveSpots.Length);
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {        
        if(health <= 0)
        {
            EnemyDestroyed();
        }

        if(Vector2.Distance(transform.position, player.position) <= followingPlayer)
        {
            if (Vector2.Distance(transform.position, player.position) > stoppingDistance)
            {
                transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
                anim.SetBool("isFollowing", true);
            }

            else if (Vector2.Distance(transform.position, player.position) < stoppingDistance && Vector2.Distance(transform.position, player.position) > retreatDistance)
            {
                transform.position = this.transform.position;
                anim.SetBool("isFollowing", false);
            }

            else if (Vector2.Distance(transform.position, player.position) < retreatDistance)
            {
                transform.position = Vector2.MoveTowards(transform.position, player.position, -speed * Time.deltaTime);
                anim.SetBool("isFollowing", false);
            }

            if (timeBtwShot <= 0)
            {
                Instantiate(projectile, transform.position, Quaternion.identity);
                timeBtwShot = startTimeBtwShots;

            }
            else
            {
                timeBtwShot -= Time.deltaTime;
            }
        }

        else
        {
            transform.position = Vector2.MoveTowards(transform.position, moveSpots[randomSpot].position, speed * Time.deltaTime);
            anim.SetBool("isFollowing", true);
            if (Vector2.Distance(transform.position, moveSpots[randomSpot].position) < 0.2f)
            {
                if(waitTime <=0)
                {
                    Debug.Log(startWaitTime);
                    randomSpot = Random.Range(0, moveSpots.Length);
                    waitTime = startWaitTime;
                }
                else
                {
                    waitTime -= Time.deltaTime;
                }
            }
        }
    }

    public void EnemyDestroyed()
    {
        Destroy(this.gameObject);
    }
}
