﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MoveScript : MonoBehaviour
{
    //OBJECT
    public static float health;
    public static float mana = 100f;
    Collider2D col;
    Rigidbody2D rb;
    Touch touch; //declare touch
    Touch touch1;
    public GameObject projectile;
    public GameObject skillProjectile;
    public GameObject skillProjectile2;
    //MOVING
    Vector3 touchPosition, whereToMove; //vector3 variable
    bool isMoving = false; //boolean when move
    float previousDistanceToTouchPos, currrentDistanceTouchPos;    
    public static float moveSpeed = 5f;
    bool moveAllowed = false;

    public float waitTime;
    public float startWaitTime;

    //LINE RENDERER
    

    // Start is called before the first frame update
    void Start()
    {
        health = 100f;
        //DECLARE
        waitTime = startWaitTime;
        
        //GET COMPONENT
        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<Collider2D>();

        //GET ANOTHER SCRIPT
        GetComponent<ProjectileScriptPlayer>();
        GetComponent<PanZoom>();
        GetComponent<skillScript>();
        GetComponent<playSkill>();
        GetComponent<skillScript2>();
        GetComponent<playSkill2>();

    }

    // Update is called once per frame
    void Update()
    {
        FindClosestEnemy();
        if (health <= 0)
        {
            SceneManager.LoadScene("GameOverScene");
            DestroyThis();
        }

        //if condition when first move
        if (isMoving == true)
        {
            //find distance between touchposition and player position
            currrentDistanceTouchPos = (touchPosition - transform.position).magnitude;
        }        

        //when input is one touch
        if (Input.touchCount == 1) 
        {
            //declare touch
            touch = Input.GetTouch(0);
            touch1 = Input.GetTouch(0);            

            //when first input touch
            if (touch.phase == TouchPhase.Began)
            {
                touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
                Collider2D touchedCollider = Physics2D.OverlapPoint(touchPosition);
                if (col == touchedCollider)
                {
                    moveAllowed = true;
                    Debug.Log("move");
                    PanZoom.active = false;
                }
            }
            
            if (touch.phase == TouchPhase.Moved)
            {
                
            }

            //when touch ended
            if(touch.phase == TouchPhase.Ended)
            {
                if (moveAllowed)
                {
                    previousDistanceToTouchPos = 0;
                    currrentDistanceTouchPos = 0;
                    isMoving = true;
                    touchPosition = Camera.main.ScreenToWorldPoint(touch1.position);
                    touchPosition.z = 0;
                    whereToMove = (touchPosition - transform.position).normalized;
                    rb.velocity = new Vector2(whereToMove.x * moveSpeed, whereToMove.y * moveSpeed);
                    moveAllowed = false;
                }
                PanZoom.active = true;                
            }

            if (skillScript2.skillActive == true)
            {
                StartCoroutine(processInput());               
            }

        }

        if (currrentDistanceTouchPos > previousDistanceToTouchPos)
        {
            isMoving = false;
            rb.velocity = Vector2.zero;
        }

        if (isMoving == true)
        {
            previousDistanceToTouchPos = (touchPosition - transform.position).magnitude;
        }        
    }    

    private IEnumerator processInput()
    {
        yield return new WaitForSeconds(0.5f);
        Debug.Log("CourotineStart");
        yield return waitForInputTouch();
    }

    private IEnumerator waitForInputTouch()
    {
        bool done = false;
        while (!done)
        {            
            if (Input.touchCount == 1)
            {
                touch = Input.GetTouch(0);
                Debug.Log("Select Area");
                touchPosition = Camera.main.ScreenToWorldPoint(touch.position);
                playSkill2.touchPositions = touchPosition;
                Instantiate(skillProjectile2, transform.position, Quaternion.identity);
                skillScript2.cooldown = false;
                Debug.Log("Skill 2 CoolDown");
                done = true;
            }

            yield return null;
        }
    }

    void FindClosestEnemy()
    {
        float distanceToClosestEnemy = 10;
        Enemy1Script[] allEnemies = GameObject.FindObjectsOfType<Enemy1Script>();

        foreach (Enemy1Script currentEnemy in allEnemies)
        {
            if(Vector2.Distance(currentEnemy.transform.position, this.transform.position) <= distanceToClosestEnemy)
            {
                if (skillScript.skillActive == true)
                {                   
                    Instantiate(skillProjectile, transform.position, Quaternion.identity);
                    skillScript.cooldown = false;
                    Debug.Log("CoolDown");
                }

                if (waitTime <= 0)
                {
                    Instantiate(projectile, transform.position, Quaternion.identity);
                    waitTime = startWaitTime;
                }

                else
                {
                    waitTime -= Time.deltaTime;
                }
            }         
        }        
    }

    public void DestroyThis()
    {
        Destroy(this.gameObject);
    }
}
