﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragToMove : MonoBehaviour
{
    bool moveAllowed;
    Collider2D col;
    Rigidbody2D rb;

    [SerializeField]
    float moveSpeed = 5f;    

    Touch touch;
    Vector3 touchPosition, whereToMove;
    bool isMoving = false;

    float previousDistanceToTouchPos, currrentDistanceTouchPos;

    void Start()
    {
        col = GetComponent<Collider2D>();
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Vector3 touchPosition = Camera.main.ScreenToWorldPoint(touch.position);

            if (touch.phase == TouchPhase.Began)
            {
                Collider2D touchedCollider = Physics2D.OverlapPoint(touchPosition);
                if (col == touchedCollider)
                {
                    moveAllowed = true;
                }
            }

            if (touch.phase == TouchPhase.Moved)
            {
                
            }

            if (touch.phase == TouchPhase.Ended)
            {
                if (isMoving == true)
                {
                    currrentDistanceTouchPos = (touchPosition - transform.position).magnitude;
                }

                if (moveAllowed)
                {
                    previousDistanceToTouchPos = 0;
                    currrentDistanceTouchPos = 0;
                    isMoving = true;
                    touchPosition.z = 0;
                    whereToMove = (touchPosition - transform.position).normalized;
                    rb.velocity = new Vector2(touchPosition.x * moveSpeed, touchPosition.y * moveSpeed);
                }

                if (currrentDistanceTouchPos > previousDistanceToTouchPos)
                {
                    isMoving = false;
                    rb.velocity = Vector2.zero;
                }

                if (isMoving == true)
                {
                    previousDistanceToTouchPos = (touchPosition - transform.position).magnitude;
                }

                moveAllowed = false;
            }
        }
    }


}
