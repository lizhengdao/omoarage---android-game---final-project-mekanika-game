﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ProjectileScriptPlayer : MonoBehaviour
{
    //MOVING
    public float speed;

    //ATTACK
    public static float power = 6.5f;
    private float distanceToClosestEnemy;
    private Enemy1Script closestEnemy;
    private Enemy1Script[] allEnemies;

    //touch
    Touch touch;
    Vector3 touchPosition;
  

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Enemy1Script>();
        GetComponent<MoveScript>();
    }

    // Update is called once per frame
    void Update()
    {
        FindClosestEnemy();        
    }

    void FindClosestEnemy()
    {
        distanceToClosestEnemy = Mathf.Infinity;
        closestEnemy = null;
        allEnemies = GameObject.FindObjectsOfType<Enemy1Script>();

        if(allEnemies == null)
        {
            SceneManager.LoadScene("WinScene");
        }

        foreach (Enemy1Script currentEnemy in allEnemies)
        {
            float distanceToEnemy = (currentEnemy.transform.position - this.transform.position).sqrMagnitude;
            if (distanceToEnemy < distanceToClosestEnemy)
            {
                distanceToClosestEnemy = distanceToEnemy;
                closestEnemy = currentEnemy;                
            }
        }

        transform.position = Vector2.MoveTowards(transform.position, closestEnemy.transform.position, speed * Time.deltaTime);
        if (transform.position.x == closestEnemy.transform.position.x && transform.position.y == closestEnemy.transform.position.y)
        {
            DestroyProjectile();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("EnemyTroops"))
        {
            other.gameObject.GetComponent<Enemy1Script>().health -= power;
            power += 0.2f;

            //FEEDBACK POSITIVE CONSTRUCTIVE
            MoveScript.moveSpeed += 1f;
            if (MoveScript.moveSpeed >= 8)
            {
                MoveScript.moveSpeed = 8;
            }

            //FEEDBACK POSITIVE DESTRUCTIVE
            other.gameObject.GetComponent<Enemy1Script>().startWaitTime -= 1f;
            if(other.GetComponent<Enemy1Script>().startWaitTime <= 0.1f)
            {
                other.GetComponent<Enemy1Script>().startWaitTime = 0.1f;
            }


            DestroyProjectile();            
        }
    }

    void DestroyProjectile()
    {
        Destroy(gameObject);
    }
}
